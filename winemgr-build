#!/bin/bash

export PROJ="$(realpath $(dirname $0))"
export MGR_MODE="build"
if [[ -f "$PROJ"/winemgr-lib.env ]]; then
  source "$PROJ"/winemgr-lib.env
else
  source /usr/share/winemgr/winemgr-lib.env
fi

## Initialization of prefix with default wine
echo "Create WINEPREFIX $WINEPREFIX"
[[ -d "$WINEPREFIX" ]] || mkdir "$WINEPREFIX"

wineboot -i -s -e
while ! [[ -f "$WINEPREFIX/system.reg" ]]; do sleep 0.1; done

# Install Winetricks
echo "Install $WINETRICKS_INSTALL"
[[ -n "$WINETRICKS_INSTALL" ]] && HOME="$HOME_ORIG" winetricks $WINETRICKS_INSTALL


# Install directX
[[ "$DXVK_INSTALL" == yes ]] && setup_dxvk.sh install --with-d3d10 --symlink
[[ "$VKD3D_INSTALL" == yes ]] && setup_vkd3d_proton.sh install --symlink

if [[ "$GALLIUM_NINE_INSTALL" == yes ]]; then
  ln -svf "/usr/lib64/d3d9-nine.dll.so" "$WINEPREFIX"/drive_c/windows/system32/d3d9-nine.dll
  $WINE "/usr/lib64/ninewinecfg.exe.so" -e
fi

# Call installers
if [[ -n "$BUILD_ENV" ]]; then
  INSTALLER_PATH="$(dirname "$(realpath "$BUILD_ENV")")"
  if [[ -n "$INSTALLER_EXE" ]]; then
    echo "Run $INSTALLER_EXE"
    cd "$WINEPREFIX"
    $WINE "$INSTALLER_PATH"/"$INSTALLER_EXE" $INSTALLER_PARAM
    cd -
  fi

  # Copy INSTALLER_COPY
  if [[ -n "$INSTALLER_COPY" ]]; then
    if [[ -d "$WINEPREFIX"/drive_c/"$INSTALLER_COPY" ]]; then
      echo "$WINEPREFIX"/drive_c/"$INSTALLER_COPY already exists, skip copy"
    else
      echo "Copy $INSTALLER_COPY"
      cp -a "$INSTALLER_PATH"/"$INSTALLER_COPY" "$WINEPREFIX"/drive_c
    fi
  fi

  if [[ -n "$INSTALLER_BASH" ]]; then
    export INSTALLER_PATH
    bash "$INSTALLER_PATH"/"$INSTALLER_BASH"
  fi

  # Save used config
  cp -v "$BUILD_ENV" "$WINEPREFIX"/winemgr-app.env
else
  touch "$WINEPREFIX"/winemgr-app.env
fi

$WINEBOOT -s
