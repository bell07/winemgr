# Abadoned

I switched to lutris wine manager and do not use this scripts anymore.

Lutris in latest version is able to detect multiple installed system-wine versions and use system-installed dxvk and vkd3d, and use system installed emulators in runners.
- Set the preferred version in in wine runner settings (revert arrow visible / version is set in ~/.config/lutris/runners/wine.yml)
- For dxvk and vkd3d create symlinks as described in https://github.com/lutris/lutris/pull/5065 and select the system-* entries
- Most wine games I install manually using "Install a Windows game from an executeable". Complex installation processes are possible using lutris installer scripts.

- Another settings I prefer to set is, disable "lutris runtime" and prefer "system libraries"
- Check the starter settings. Any (emulator) starter have setting for "Custom executable", that allow use system installed emulator.

# 

# Simple Wine bottle manager

The Manager provides script to build up a bottle and a script to run apps inside the bottle.

Most wine managers download wine and other parts prebuilt from web, ignoring local installed software.
I am Gentoo user, therefore  I like to use the locally compiled software optimized for my needs:

```
app-emulation/dxvk
app-emulation/gallium-nine-standalone
app-emulation/vkd3d-proton
app-emulation/wine-staging
app-emulation/wine-lutris
app-emulation/wine-proton
...
```

For that reason I wrote this simple manager. The manager is able to use `winetricks` for additional components not available in portage ;-)

Default bottles base directory and other values can be set in `/etc/winemgr.env` file.

```
# Path the bottles are stored
BOTTLES=~/games/wine_bottles

# Default Wine setup
export WINE="wine-staging"
export WINEFSYNC=1
export WINEARCH=win64
```


## Create new bottle
New bottles creation is using `winemgr-build` script.

```
winemgr-build [bottlename] [path-to/definition-file.env] [installer exe file and additional installer parameter]
```
If no bottlename is given, the path name of definition file is used.
If no defnition-file.env is given, an empty bottle is created without any intallation steps.

The definition file can contain next settings:

```
# Setting overrides for winemgr.env, if necessary
#export WINE="wine-staging"

## Install listed winetricks modules inside bottle
#WINETRICKS_INSTALL="win7 vcrun2012"

## Install app-emulation/gallium-nine-standalone for DirectX 9 Driver
#GALLIUM_NINE_INSTALL=yes

## Install app-emulation/dxvk (Vulkan based DirectX 9-11)
#DXVK_INSTALL=yes

## Install vkd3d_proton (Proton Direct3D 12)
#VKD3D_INSTALL=yes

## Installer to be called
#INSTALLER_EXE=setup.exe

## Parameter for installer
#INSTALLER_PARAM=''

## Copy directory to c:\
#INSTALLER_COPY=

## Bash script is executed for installation steps
#INSTALLER_BASH=setup.sh

## Default run command for wiemgr-run
RUN_EXE='C:\\Program Files (x86)\\game\\game.exe'
RUN_PARAM=''

````

The idea is the definition env file (name proposal `winemgr-def.env`) is placed inside the installation folder beside the setup.exe.
The builder creates new wineprefix, do the installation steps and copy the definition file inside the new prefix, named `winemgr-app.env` to be used with winemgr-run script.
If defined, the steps are executed in the next order:

- call INSTALLER_EXE file using wine
- copy recursive INSTALLER_COPY into drive_c directory
- run INSTALLER_BASH using bash

In INSTALLER_BASH script the INSTALLER_PATH ist exported, that points to the installation source folder. For target the WINEPREFIX is available.


## Run app in bottle

Run apps installed in bottle can be done using 'winemgr-run' script.

```
winemgr-run [bottlename] [path-to/application.exe and additional application parameter]
```

If bottlename is not given, the script determines them by path-to/application.exe searching for `/drive_c/`.
If path-to/application.exe is not given, the default application is called, defined by `RUN_EXE` and `RUN_PARAM`
